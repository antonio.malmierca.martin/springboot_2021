package es.edu.alter.practica0.modelo.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.edu.alter.practica0.modelo.Lagarto;
import es.edu.alter.practica0.modelo.Papel;
import es.edu.alter.practica0.modelo.Piedra;
import es.edu.alter.practica0.modelo.PiedraPapelTijeraFactory;
import es.edu.alter.practica0.modelo.Spock;
import es.edu.alter.practica0.modelo.Tijera;

class PiedraPapelTijeraFactoryTest {
	
	//1.-lote de pruebas
	PiedraPapelTijeraFactory piedra, papel, tijera, lagarto, spock;
	
	
	
	@BeforeEach
	void setUp() throws Exception {
		
		//se ejecuta antes de cada prueba
		piedra 	= new Piedra();
		papel 	= new Papel();
		tijera 	= new Tijera();
		lagarto = new Lagarto();
		spock 	= new Spock();
		
	}

	@AfterEach
	void tearDown() throws Exception {
		
		//se ejecuta despues de cada prueba
		piedra 	= null;
		papel 	= null;
		tijera 	= null;
		lagarto = null;
		spock 	= null;
	}

	
	@Test
	void testGetInstancePapel() {
		assertEquals("papel", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PAPEL)
														.getNombre().toLowerCase());
	}
	
	@Test
	void testGetInstancePiedra() {
		assertEquals("piedra", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PIEDRA)
														.getNombre().toLowerCase());
	}
	
	@Test
	void testGetInstanceTijera() {
		assertEquals("tijera", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.TIJERA)
														.getNombre().toLowerCase());
	}
	
	@Test
	void testGetInstanceLagarto() {
		assertEquals("lagarto", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.LAGARTO)
														.getNombre().toLowerCase());
	}
	
	@Test
	void testGetInstanceSpock() {
		assertEquals("spock", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.SPOCK)
														.getNombre().toLowerCase());
	}
	
	
	//casos PIEDRA
	@Test
	void testCompararPiedraPierdePapel() {
		assertEquals(-1, piedra.comparar(papel));
		assertEquals("piedra perdi� con papel", piedra.getDescripcionResultado().toLowerCase());
		
	}
	
	@Test
	void testCompararPiedraPierdeSpock() {
		assertEquals(-1, piedra.comparar(spock));
		assertEquals("piedra perdi� con spock", piedra.getDescripcionResultado().toLowerCase());
		
	}
	
	@Test
	void testCompararPiedraGanaATijera() {
		//TODO testCompararPiedraGanaATijera
		assertEquals(1, piedra.comparar(tijera));
		assertEquals("piedra le gano a tijera", piedra.getDescripcionResultado().toLowerCase());
		
	}
	
	@Test
	void testCompararPiedraGanaALagarto() {
		//TODO testCompararPiedraGanaALagarto
		assertEquals(1, piedra.comparar(lagarto));
		assertEquals("piedra le gano a lagarto", piedra.getDescripcionResultado().toLowerCase());
		
	}
	
	@Test
	void testCompararPiedraEmpataConPiedra() {
		//TODO agregar texto empate en todos los lados
		assertEquals(0, piedra.comparar(piedra));
		assertEquals("piedra empat� con piedra", piedra.getDescripcionResultado().toLowerCase());
	}
	
	
	//casos PAPEL
	@Test
	void testCompararPapelGanaConPiedra() {
		//TODO testCompararPapelGanaConPiedra
		assertEquals(1, papel.comparar(piedra));
		assertEquals("papel le gano a piedra", papel.getDescripcionResultado().toLowerCase());
		
	}
	
	void testCompararPapelGanaASpock() {
		//TODO testCompararPapelGanaASpock
		assertEquals(1, papel.comparar(spock));
		assertEquals("papel le gano a spock", papel.getDescripcionResultado().toLowerCase());
		
	}
	
	@Test
	void testCompararPapelPierdeConTijera() {
		//TODO testCompararPapelPierdeConTijera
		assertEquals(-1, papel.comparar(tijera));
		assertEquals("papel perdi� con tijera", papel.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararPapelPierdeConLagarto() {
		//TODO testCompararPapelPierdeConLagarto
		assertEquals(-1, papel.comparar(lagarto));
		assertEquals("papel perdi� con lagarto", papel.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararPapelEmpataConPapel() {
		//TODO agregar texto empate en todos los lados
		assertEquals(0, papel.comparar(papel));
		assertEquals("papel empat� con papel", papel.getDescripcionResultado().toLowerCase());
		
	}
	
	
	//casos TIJERA
	@Test
	void testCompararTijeraGanaAPapel() {
		//TODO testCompararTijeraGanaAPapel
		assertEquals(1, tijera.comparar(papel));
		assertEquals("tijera le gano a papel", tijera.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararTijeraGanaALagarto() {
		//TODO testCompararTijeraGanaALagarto
		assertEquals(1, tijera.comparar(lagarto));
		assertEquals("tijera le gano a lagarto", tijera.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararTijeraPierdeConPiedra() {
		//TODO testCompararTijeraPierdeConPiedra
		assertEquals(-1, tijera.comparar(piedra));
		assertEquals("tijera perdi� con piedra", tijera.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararTijeraPierdeConSpock() {
		//TODO testCompararTijeraPierdeConSpock
		assertEquals(-1, tijera.comparar(spock));
		assertEquals("tijera perdi� con spock", tijera.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararTijeraEmpataConTijera() {
		//TODO agregar texto empate en todos los lados
		assertEquals(0, tijera.comparar(tijera));
		assertEquals("tijera empat� con tijera", tijera.getDescripcionResultado().toLowerCase());
		
	}
	
	//casos LAGARTO
	@Test
	void testCompararLagartoGanaAPapel() {
		//TODO testCompararLagartoGanaAPapel
		assertEquals(1, lagarto.comparar(papel));
		assertEquals("lagarto le gan� a papel", lagarto.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararLagartoGanaASpock() {
		//TODO testCompararLagartoGanaASpock
		assertEquals(1, lagarto.comparar(spock));
		assertEquals("lagarto le gan� a spock", lagarto.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararLagartoPierdeConPiedra() {
		//TODO testCompararLagartoPierdeConPiedra
		assertEquals(-1, lagarto.comparar(piedra));
		assertEquals("lagarto perdi� con piedra", lagarto.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararLagartoPierdeConTijera() {
		//TODO testCompararLagartoPierdeConTijera
		assertEquals(-1, lagarto.comparar(tijera));
		assertEquals("lagarto perdi� con tijera", lagarto.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararLagartoEmpataConLagarto() {
		//TODO agregar texto empate en todos los lados
		assertEquals(0, lagarto.comparar(lagarto));
		assertEquals("lagarto empat� con lagarto", lagarto.getDescripcionResultado().toLowerCase());
		
	}
	
	//casos SPOCK
	@Test
	void testCompararSpockGanaATijera() {
		//TODO testCompararLagartoGanaAPapel
		assertEquals(1, spock.comparar(tijera));
		assertEquals("spock le gan� a tijera", spock.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararSpockGanaAPiedra() {
		//TODO testCompararLagartoGanaASpock
		assertEquals(1, spock.comparar(piedra));
		assertEquals("spock le gan� a piedra", spock.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararSpockPierdeConLagarto() {
		//TODO testCompararLagartoPierdeConPiedra
		assertEquals(-1, spock.comparar(lagarto));
		assertEquals("spock perdi� con lagarto", spock.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararSpockPierdeConPapel() {
		//TODO testCompararLagartoPierdeConTijera
		assertEquals(-1, spock.comparar(papel));
		assertEquals("spock perdi� con papel", spock.getDescripcionResultado().toLowerCase());
		
		
	}
	
	@Test
	void testCompararSpockEmpataConSpock() {
		//TODO agregar texto empate en todos los lados
		assertEquals(0, spock.comparar(spock));
		assertEquals("spock empat� con spock", spock.getDescripcionResultado().toLowerCase());
		
	}
	
	
	

}
