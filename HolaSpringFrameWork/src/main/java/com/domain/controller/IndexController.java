package com.domain.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.domain.modelo.Alumno;
import com.domain.modelo.dao.AlumnoDao;

import es.edu.alter.practica0.modelo.OptionsDefined;
import es.edu.alter.practica0.modelo.PiedraPapelTijeraFactory;

@Controller
public class IndexController {
	
	@RequestMapping("/home")
	public String goIndex() {
		
		return "Index";
	}
	
	@RequestMapping("/")
	public String getPresentacion() {
		
		return "Presentacion";
	}
	
//	@RequestMapping("/listado")
//	public String goListado(Model model) {
//		
//		List<String> alumnos = new ArrayList<String>();
//		alumnos.add("Juan");
//		alumnos.add("Pedro");
//		alumnos.add("Jos�");
//		model.addAttribute("titulo", "Listado de alumnos");
//		model.addAttribute("profesor", "Gabriel Casas");
//		model.addAttribute("alumnos", alumnos);
//		return "Listado";
//	}
	
	@RequestMapping("/listado")
	public String leer(Model pModel) throws ClassNotFoundException, SQLException {
		
		List<com.domain.modelo.Model> lista = new ArrayList<com.domain.modelo.Model>();
		AlumnoDao alu = new AlumnoDao();
		alu.leer(new Alumno());
		
		lista = alu.leer(new Alumno());
		pModel.addAttribute("lista", lista);
		
		
		return "Listado";
		
	}
	
	@RequestMapping("/juego/{nombre}")
	public String goListado(@PathVariable("nombre") String nombre,  Model model) {
	//se debera crear un arrayList con las opciones piedra, papel y tijera
	List<String> opciones = new ArrayList<String>();
	opciones.add("Piedra");
	opciones.add("Papel");
	opciones.add("Tijera");
	opciones.add("Lagarto");
	opciones.add("Spock");
	//completar como correspondan en este punto existen 3 opciones y hay una que es la
	//que mas megusta
	//enviar las opciones a trav�s de
	model.addAttribute("nombre",nombre);
	model.addAttribute("opciones",opciones);
	
	return "PiedraPapelTijera";
	}
	
	@RequestMapping("/juego/resolverJuego")
	public String goResultado(Model model) {
	//debo obtener la comparaci�n
	//determinar si el jugador gano, perdi� o empato
	//se debe tener en cuenta que el ordenador elige una opci�n al hazar
	//utilizar el patr�n factory
	//enviar los resultados
		Map<Integer, String> optionsMap = new HashMap<Integer, String>();
		
		optionsMap.put(OptionsDefined.PIEDRA.getId(), OptionsDefined.PIEDRA.getName());
		optionsMap.put(OptionsDefined.PAPEL.getId(), OptionsDefined.PAPEL.getName());
		optionsMap.put(OptionsDefined.TIJERA.getId(), OptionsDefined.TIJERA.getName());
		optionsMap.put(OptionsDefined.LAGARTO.getId(), OptionsDefined.LAGARTO.getName());
		optionsMap.put(OptionsDefined.SPOCK.getId(), OptionsDefined.SPOCK.getName());
		
		model.addAttribute("options",optionsMap);
		
		
		
	return "resolverJuegoPiedraPapelTijera";
	}
	
	@RequestMapping("/juego/resultados")
	public String checkResult(@RequestParam("options")int option, Model model) {
		
		PiedraPapelTijeraFactory papelTijeraFactoryGame;
		PiedraPapelTijeraFactory papelTijeraFactoryUser;
		
		int value = 0;
		
		while  (value == 0) {
			value = (int) (Math.random() * 6);
		}
		
		//Math.random()*100%5 +1 solucion miguel para seleccionar el numero mas efeciva
		
		papelTijeraFactoryGame = PiedraPapelTijeraFactory.getInstance(value);
		papelTijeraFactoryUser = PiedraPapelTijeraFactory.getInstance(option);
		
		String machine = "valor maquina  " + papelTijeraFactoryGame.getNombre();
		String player = "valor jugador  " + papelTijeraFactoryUser.getNombre();
		
		papelTijeraFactoryUser.comparar(papelTijeraFactoryGame);
		
		String response = "";
		
		model.addAttribute("jugador", player);
		model.addAttribute("pc", machine);
		model.addAttribute("result", papelTijeraFactoryUser.getDescripcionResultado());
		
		return "resultados";
	}

	
	
}
