package com.domain.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.domain.modelo.Alumno;
import com.domain.modelo.Model;
import com.domain.util.ConnectionManager;

public class AlumnoDao implements DAO {
	
	public AlumnoDao(){
		
	}

	@Override
	public void agregar(Model pModel) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();
		
		String sql = new String("insert into alumnos(alu_nombre, alu_apellido, alu_conocimientos,alu_git) values(?,?,?,? )");
		
		Alumno alu =(Alumno) pModel;
		PreparedStatement stm = con.prepareStatement(sql);
		stm.setString(1, alu.getNombre());
		stm.setString(2, alu.getApellido());
		stm.setString(3, alu.getEstudios());
		stm.setString(4, alu.getLinkARepositorio());		
		stm.execute();
		
	}

	@Override
	public void modificar(Model pModel) throws ClassNotFoundException, SQLException {
	ConnectionManager.conectar();
	Connection con = ConnectionManager.getConnection();

	String sql = new String("update alumnos set alu_nombre=?,alu_apellido=?,alu_conocimientos=?,alu_git=? where alu_id=?");

	Alumno alu =(Alumno) pModel;
	PreparedStatement stm = con.prepareStatement(sql);
	stm.setString(1, alu.getNombre());
	stm.setString(2, alu.getApellido());
	stm.setString(3, alu.getEstudios());
	stm.setString(4, alu.getLinkARepositorio());
	stm.setInt(5, alu.getCodigo());
	stm.execute();

	}



	@Override
	public void eliminar(Model pModel) throws ClassNotFoundException, SQLException {
	ConnectionManager.conectar();
	Connection con = ConnectionManager.getConnection();

	String sql = new String("delete from alumnos where alu_id=?");

	Alumno alu =(Alumno) pModel;
	PreparedStatement stm = con.prepareStatement(sql);
	stm.setInt(1, alu.getCodigo());
	stm.execute();



	}

	@Override
	public List<Model> leer(Model pModel) throws ClassNotFoundException, SQLException {

		List<Model> lista = new ArrayList<Model>();
		ConnectionManager.conectar();
		Connection conn = ConnectionManager.getConnection();
		StringBuilder sql = new StringBuilder("select * from alumnos");
				
		Alumno alumno = (Alumno) pModel;
		if(alumno.getCodigo()>0) {
			sql.append(" where alu_id=?");			
		}
	
		PreparedStatement stm = conn.prepareStatement(sql.toString());
		
		if(alumno.getCodigo()>0) {
			stm.setInt(1, alumno.getCodigo());			
		}
		
		ResultSet rs = stm.executeQuery(); 

		while (rs.next()) {
			
			alumno = new Alumno();
			alumno.setNombre(rs.getString("alu_nombre"));
			alumno.setApellido(rs.getString("alu_apellido"));
			alumno.setEstudios(rs.getString("alu_conocimientos"));
			alumno.setLinkARepositorio(rs.getString("alu_git"));
			lista.add(alumno);
		}
		return lista;
	}


}
