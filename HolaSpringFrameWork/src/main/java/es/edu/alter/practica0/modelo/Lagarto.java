package es.edu.alter.practica0.modelo;

public class Lagarto extends PiedraPapelTijeraFactory {

	public Lagarto() {
		this("lagarto", LAGARTO );
	}
	
	public Lagarto(String pNom, int pNum) {
		super("lagarto", LAGARTO);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum == LAGARTO;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedraPapelTijera) {
		int resul=0;
		switch (pPiedraPapelTijera.getNumero()) {
		
		//ganar
		case PAPEL:
		case SPOCK:	
			resul=1;
			this.descripcionResultado ="lagarto le gan� a " + pPiedraPapelTijera.getNombre();
			break ;

		//perder	
		case PIEDRA:
		case TIJERA:	
			resul=-1;
			this.descripcionResultado ="lagarto perdi� con " + pPiedraPapelTijera.getNombre();
			break;
		default:	
			resul=0;
			this.descripcionResultado ="lagarto empat� con " + pPiedraPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
