package es.edu.alter.practica0.modelo;

public class Papel extends PiedraPapelTijeraFactory {

	public Papel() {
		this("papel", PAPEL );
	}
	
	public Papel(String pNom, int pNum) {
		super("papel", PAPEL);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isMe(int pNum) {
		// TODO Auto-generated method stub
		return pNum == PAPEL;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		
		//ganar
		case PIEDRA:
		case SPOCK:
			resul=1;
			this.descripcionResultado ="Papel le gano a " + pPiedPapelTijera.getNombre();
			break ;

		//perder	
		case TIJERA:
		case LAGARTO:
			
			resul=-1;
			this.descripcionResultado ="Papel perdi� con " + pPiedPapelTijera.getNombre();
			break;
		default:	
			resul=0;
			this.descripcionResultado ="Papel empat� con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
