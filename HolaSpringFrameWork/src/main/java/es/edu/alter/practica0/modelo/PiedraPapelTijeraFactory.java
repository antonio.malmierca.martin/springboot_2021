package es.edu.alter.practica0.modelo;

import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTijeraFactory {

	//constantes
	public final static int PIEDRA 	= 1;
	public final static int PAPEL 	= 2;
	public final static int TIJERA 	= 3;
	public final static int LAGARTO = 4;
	public final static int SPOCK	= 5;
	
	
	//atributos
	protected String 	descripcionResultado;
	private static List<PiedraPapelTijeraFactory> 	elementos;
	protected String 	nombre;
	protected int 		numero;
	
	//contructores
	public PiedraPapelTijeraFactory(String pNom, int pNum) {
		
		nombre = pNom;
		numero = pNum;
		
	}

	//getter y setter
	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	public void setDescripcionResultado(String descripcionResultado) {
		this.descripcionResultado = descripcionResultado;
	}

	public static List<PiedraPapelTijeraFactory> getElementos() {
		return elementos;
	}

	public static void setElementos(List<PiedraPapelTijeraFactory> elementos) {
		PiedraPapelTijeraFactory.elementos = elementos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	//metodos de negocio
	public abstract boolean isMe(int pNum);
	public abstract int comparar(PiedraPapelTijeraFactory pPiedraPapelTijera);
	
	public static PiedraPapelTijeraFactory getInstance(int pNumero) {
		//el corazon del factory
		//1ro el padre reconoce a todos sus hijos
		elementos = new ArrayList<PiedraPapelTijeraFactory>();
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());
		
		elementos.add(new Lagarto());
		elementos.add(new Spock());
		
		
		
		// es todo el codigo va a ser siemrpe el misom
		for (PiedraPapelTijeraFactory piedraPapelTijeraFactory : elementos) {
			if(piedraPapelTijeraFactory.isMe(pNumero))
				return piedraPapelTijeraFactory;
			 	
		}
		
				
		return null;
	}
}
