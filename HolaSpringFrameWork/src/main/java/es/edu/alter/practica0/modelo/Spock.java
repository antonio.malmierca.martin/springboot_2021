package es.edu.alter.practica0.modelo;

public class Spock extends PiedraPapelTijeraFactory {

	public Spock() {
		this("spock", SPOCK );
	}
	
	public Spock(String pNom, int pNum) {
		super("spock", SPOCK);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum == SPOCK;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedraPapelTijera) {
		int resul=0;
		switch (pPiedraPapelTijera.getNumero()) {
		
		//ganar
		case TIJERA:
		case PIEDRA:	
			resul=1;
			this.descripcionResultado ="spock le gan� a " + pPiedraPapelTijera.getNombre();
			break ;

		//perder	
		case PAPEL:
		case LAGARTO:	
			resul=-1;
			this.descripcionResultado ="spock perdi� con " + pPiedraPapelTijera.getNombre();
			break;
		default:	
			resul=0;
			this.descripcionResultado ="spock empat� con " + pPiedraPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
