package es.edu.alter.practica0.modelo;

public class Piedra extends PiedraPapelTijeraFactory{

	public Piedra() {
		this("piedra", PIEDRA );
	}
	
	public Piedra(String pNom, int pNum) {
		super("piedra", PIEDRA);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isMe(int pNum) {
		// TODO Auto-generated method stub
		
		return pNum == PIEDRA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		
		//ganar
		case TIJERA:
		case LAGARTO:	
			resul=1;
			this.descripcionResultado ="Piedra le gano a " + pPiedPapelTijera.getNombre();
			break ;

		//perder	
		case PAPEL:	
		case SPOCK:	
			resul=-1;
			this.descripcionResultado ="Piedra perdi� con " + pPiedPapelTijera.getNombre();
			break;
		default:	
			resul=0;
			this.descripcionResultado ="Piedra empat� con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
	

}
