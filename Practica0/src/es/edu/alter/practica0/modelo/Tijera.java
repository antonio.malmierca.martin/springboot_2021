package es.edu.alter.practica0.modelo;

public class Tijera extends PiedraPapelTijeraFactory {

	public Tijera() {
		this("tijera", TIJERA );
	}
	
	public Tijera(String pNom, int pNum) {
		super("tijera", TIJERA);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isMe(int pNum) {
		// TODO Auto-generated method stub
		
		return pNum == TIJERA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		
		//ganar
		case PAPEL:
		case LAGARTO:	
			resul=1;
			this.descripcionResultado ="Tijera le gano a " + pPiedPapelTijera.getNombre();
			break ;

		//perder	
		case PIEDRA:
		case SPOCK:	
			resul=-1;
			this.descripcionResultado ="Tijera perdi� con " + pPiedPapelTijera.getNombre();
			break;
		default:	
			resul=0;
			this.descripcionResultado ="Tijera empat� con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;

	}

}
