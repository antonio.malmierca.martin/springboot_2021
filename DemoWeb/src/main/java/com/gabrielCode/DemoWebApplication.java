package com.gabrielCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gabrielCode.service.IpersonaService;

@SpringBootApplication
public class DemoWebApplication implements CommandLineRunner {
    private static Logger log = LoggerFactory.getLogger(DemoWebApplication.class);
    
    @Autowired
    private IpersonaService service;
	 
	public static void main(String[] args) {
		SpringApplication.run(DemoWebApplication.class, args);
	}
	@Override
	public void run(String... args) throws Exception {
		//service = new PersonaServiceImpl();
		service.registrarHandler("Gabriel");
	}
}